#!/usr/bin/env sh
GITHUB_REPOSITORY_URL="https://github.com/helm/helm"

LATEST_RELEASE_TAG="$(./get-latest-release-tag.sh "${GITHUB_REPOSITORY_URL}")"
LATEST_VERSION="$(echo "${LATEST_RELEASE_TAG}" | awk -F'^v?' '{print $2}')"

echo "helm: LATEST_VERSION=${LATEST_VERSION}"
URL="https://get.helm.sh/helm-v${LATEST_VERSION}-linux-amd64.tar.gz"
echo "helm: URL=${URL}"
curl -sL "${URL}" | tar zxv --no-same-owner -C "$(pwd)" linux-amd64/helm --strip=1

test -f "helm"
