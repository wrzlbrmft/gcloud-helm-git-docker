# docker-gcloud-helm-git

Google Cloud SDK with Helm and Git for CI/CD pipelines in Docker

Based on https://hub.docker.com/r/google/cloud-sdk . Just adding Git and Helm.

```
docker pull wrzlbrmft/gcloud-helm-git:latest
```

See also:

  * https://cloud.google.com/sdk/
  * https://helm.sh/
  * https://git-scm.com/
  * https://hub.docker.com/r/wrzlbrmft/gcloud-helm-git/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
