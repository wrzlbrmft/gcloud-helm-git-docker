FROM google/cloud-sdk:alpine

RUN gcloud components install kubectl

RUN apk add --no-cache \
        ca-certificates \
        git

COPY helm /usr/local/bin/
